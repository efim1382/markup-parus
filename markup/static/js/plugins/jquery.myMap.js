function MyMap(options) {
  this._options = options; // Параметры карты
  this._work = false; // Признак инициализации
  this._scrollTimer = null;
  this._mapElement = null;

  /*
  * Находит на странице карту с заданным id
  */
  this._findMap = function() {
    this._mapElement = $('#' + this._options.idMap);
  }

  /*
  * Проверяет на наличие карты в viewport
  */
  this._checkMap = function() {
    if (this._work) {
      return;
    }

    this._findMap();

    if ( $(this._mapElement).is( ':in-viewport' ) ) {
      this._work = true;
      this._init();
      return;
    }

    // if ($.inViewport($(this._mapElement))) {
    //   this._work = true;
    //   this._init();
    //   return;
    // }

    this._checkViewport();
  }

  /*
  * Вызывает проверку на viewport
  */
  this._checkViewport = function() {
    clearTimeout(this._scrollTimer);
    var $this = this;

    this._scrollTimer = setTimeout(function() {
      func.call($this);
    }, 50);

    function func() {
      this._checkMap();
    }
  }

  /*
  * Инициализирует карту с заданными параметрами
  */
  this._init = function() {
    $(this._mapElement).css('background-image', 'url("/static/images/content/preloader.svg")');
    $(this._mapElement).css('background-color', '#fff');
    $(this._mapElement).css('background-repeat', 'no-repeat');
    $(this._mapElement).css('background-position', '50% 50%');
    $(this._mapElement).css('background-size', '4rem 4rem');
    var script = document.createElement('script');

    script.src = '//api-maps.yandex.ru/2.1/?lang=ru-RU';
    document.body.appendChild(script);

    var $this = this;
    script.onload = function() {
      ymaps.ready(function() {
        initMap.call($this);
      });
    };

    function initMap() {
      var mapOptions = {
        center: this._options.center,
        zoom: this._options.zoom,
        controls: ['smallMapDefaultSet']
        // behaviors: ["scrollZoom"]
      };

      this._mapElement = new ymaps.Map(this._options.idMap, mapOptions);
      initPlacemarks.call($this);
      this._mapElement.behaviors.disable(['scrollZoom']);
    };

    function initPlacemarks() {
      var myCollection = new ymaps.GeoObjectCollection({}, this._options.icon);

      $(this._options.placemarks).each(function(i, element) {
        myCollection.add(new ymaps.Placemark([element.coords[0], element.coords[1]], {
          balloonContentHeader: element.balloonContentHeader,
          balloonContentBody: element.balloonContentBody
        }));
      })

      this._mapElement.geoObjects.add(myCollection);
    }
  };

  /*
  * Перемещение карты между метками, в зависимости от id метки
  */
  this.goTo = function(id) {
    var coordinate = this._options.placemarks[id].coords;

    if (!coordinate) {
      return;
    }

    // this._mapElement.panTo([coordinate[0], coordinate[1] - 0.005], {
    this._mapElement.panTo(coordinate, {
      flying: true
    })
  };

  this.start = function() {
    this._checkViewport();
  }
}