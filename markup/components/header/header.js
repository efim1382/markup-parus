$(function () {
  $('.form__field_phone').mask('+7 (999) 999-99-99');

  let open = $('#headerButton');
  let close = $('#closeButton');
  let navigation = $('#headerNavigation');
  let navLinks = $('.headerNavigation__link');

  navLinks.each((index, element) => {
    element.onclick = event => {
      let href = $(event.target).attr('href');
      let block = $(`${href}`);
      let blockOffsetTop = block.offset().top;
      $('body').animate({ scrollTop: blockOffsetTop }, 1100);
      $('html').animate({ scrollTop: blockOffsetTop }, 1100);
      navigation.removeClass('headerNavigation__opened');
    };
  });

  open.on('click', event => {
    navigation.addClass('headerNavigation__opened');
  });

  close.on('click', event => {
    navigation.removeClass('headerNavigation__opened');
  });
});
