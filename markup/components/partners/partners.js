(function ($) {
  $(function () {
    $('.owl-5').owlCarousel({
      navigation: true,
      responsive: true,
      responsiveRefreshRate: 200,
      responsiveBaseWidth: window,
      items: 5,
      itemsCustom: false,
      itemsDesktop: [1199, 4],
      itemsDesktopSmall: [980, 3],
      itemsTable: [768, 2],
      itemsTabletSmal: false,
      itemsMobile: [479, 1]
    });
  });
})(jQuery);
