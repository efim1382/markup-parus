(function ($) {
  $(function () {
    /* eslint-disable */
    if (typeof contactsMapOptions == 'undefined') {
      return;
    }

    let map = new MyMap(contactsMapOptions[0]);
    map.start();
    /* eslint-enable */
  });
})(jQuery);
