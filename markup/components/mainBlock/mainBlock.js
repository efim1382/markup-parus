function isFormValid(form) {
  let elements = form.elements;
  let isValid = true;

  $(elements).each((index, element) => {
    element = $(element);

    if (element.hasClass('form__field') && !element.val()) {
      isValid = false;
    }
  });

  return isValid;
}

function resetForm(form) {
  let inputs = $(form).find('input');

  inputs.each((index, element) => {
    $(element).val('');
  });
}

function sendForm(form) {
  let formData = new FormData(form);
  let formName = $(form).data('name');

  formData.append('formName', formName);

  let xhr = new XMLHttpRequest();
  xhr.open('post', '/');
  xhr.send(formData);
}

window.onload = () => {
  $('.fancybox').fancybox();
};

$(function () {
  let page = $('#page');
  let preloader = $('#preloader');

  setTimeout(() => {
    page.removeClass('page_hidden');
    preloader.addClass('preloader_hidden');
  }, 500);
});

$(function () {
  let forms = $(document.forms);

  forms.each((index, element) => {
    let form = $(element);
    let submit = form.find('button');

    form.on('submit', event => {
      return false;
    });

    submit.on('click', event => {
      if (isFormValid(element)) {
        sendForm(element);
        form.removeClass('invalid');

        if (form.hasClass('popupForm')) {
          let popup = form.parents('.fancybox-skin');
          let close = popup.find('.fancybox-close');
          close.click();
        }

        let informationPopup = $('#informationPopup');
        informationPopup.removeClass('informationPopup__hidden');

        setTimeout(() => {
          informationPopup.addClass('informationPopup__hidden');
        }, 3000);

        resetForm(form);

        return;
      }

      form.addClass('invalid');
    });
  });
});
