(function ($) {
  $(function () {
    $('.owl-3').owlCarousel({
      navigation: true,
      responsive: true,
      responsiveRefreshRate: 200,
      responsiveBaseWidth: window,
      items: 4,
      itemsCustom: false,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [980, 2],
      itemsTable: [768, 1],
      itemsTabletSmal: false,
      itemsMobile: [479, 1]
    });
  });
})(jQuery);
