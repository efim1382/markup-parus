(function ($) {
  $(function () {
    $('.owl').owlCarousel({
      navigation: true,
      responsive: true,
      responsiveRefreshRate: 200,
      responsiveBaseWidth: window,
      items: 1,
      itemsCustom: false,
      itemsDesktop: [1199, 1],
      itemsDesktopSmall: [980, 1],
      itemsTable: [768, 1],
      itemsTabletSmal: false,
      itemsMobile: [479, 1]
    });
  });
})(jQuery);
