$(function () {
  $('.popup-fancy').fancybox({
    tpl: {
      wrap: '<div class="popup-wrap fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
    }
  });
});
