function animateItems() {
  let items = $('.howtoworkList__item');
  let time = 50;

  items.each((index, element) => {
    element = $(element);

    setTimeout(() => {
      element.addClass('animated');
    }, time);

    time += 250;
  });
}

function animateAbout() {
  let howtowork = $('.howtowork');

  if ( !howtowork.is(':in-viewport') || howtowork.hasClass('animated')) {
    return;
  }

  if ($('.page_hidden').length === 1) {
    setTimeout(() => {
      animateItems();
    }, 900);
  } else {
    animateItems();
  }

  howtowork.addClass('animated');
}

$(function () {
  setTimeout(animateAbout, 50);

  window.onscroll = event => {
    animateAbout();
  };
});
